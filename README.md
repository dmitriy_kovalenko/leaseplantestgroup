# Kovalenko Dmitriy test task for LeasePlan

## My fixes:
 1. Fixed maven error for maven-failsafe-plugin with by adding <pluginManagement> tag
 2. Added app base url to maven variable. Reading it to constant
 3. Updated cucumber step to acting as first face. I call endpoint
 4. Rename step, he sees the results displayed for apple > I verify response status is: {int}
 5. Fixed step he sees the results displayed for mango, and made it parametric
 6. Fixed assertion for product titles, iterating over all product results
 7. Added missing scenario title
 8. Styling: fixed feature name to SearchForProduct.feature, deleted not used classes and methods
 9. Fixed typos in text of the feature
 10. Added yaml file and settings file to running tests in CI
 11. ~~Updated maven libraries to latest versions~~. Returned back to old versions because newer version doesn't work
 12. Removed skip tests param. That was not fair!
 13. Added reporting artifact to yaml
 14. Updated readme file
 15. Thanks for opportunity!

### Getting started with Serenity and Cucumber

Serenity BDD Is a framwork that allows to link sceanrios steps with automation tests implementation 

### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
     + search                  Feature file subdirectories
                SearchForProduct.feature
```

## The sample scenario
Each scenario should have name describing the test. 
"Given" key word used for test preconditions
"When" key word used for test action
"Then" key word used for verifications of test results
"And" and "But" are helping keywords to link few steps of similar type

```Gherkin
  Scenario: search for a not existing product
    When I call search endpoint "car"
    Then I verify that response status is: 404
    And I verify error message is correct
```

### The Action Classes implementation.
Each step must be implemented as Java code.
The glue code in this version looks this:

```java
        @When("I call search endpoint {string}")
        public void heCallsEndpoint(String searchParam) {
            SerenityRest.given().get(Configs.BASE_URL + "search/test/" + searchParam);
        }
```

### Running tests
You can run single test in your IDE with cucumber plugin or run all tests with maven
It will run with default environment url. 

```
    $ mvn test
```

To run with custom environment url give it as a param
```
    $ mvn test -Dwebdriver.base.url=https://waarkoop-server.herokuapp.com/
```

## Generating the reports
The test results will be recorded in the `target/site/serenity` directory.
In GitLab CI you can download test report as artifact archive and review HTML report file