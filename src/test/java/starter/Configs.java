package starter;

public class Configs {
    public static final String BASE_URL = System.getenv().getOrDefault("webdriver.base.url", "https://waarkoop-server.herokuapp.com/api/v1/");
}
