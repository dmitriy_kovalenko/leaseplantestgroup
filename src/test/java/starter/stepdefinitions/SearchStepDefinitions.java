package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import starter.Configs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {
    @When("I call search endpoint {string}")
    public void heCallsEndpoint(String searchParam) {
        SerenityRest.given().get(Configs.BASE_URL + "search/test/" + searchParam);
    }

    @Then("I verify that response status is: {int}")
    public void heSeesTheResultsDisplayedForApple(int expectedStatus) {
        restAssuredThat(response -> response.statusCode(expectedStatus));
    }

    @And("I verify that response contains products with title {string}")
    public void iVerifyThatResultContainsProduct(String expectedResult) {
        Response response = SerenityRest.then().extract().response();
        List<String> responseTitles = response.jsonPath().getList("title");
        SoftAssertions softAssertions = new SoftAssertions();

        responseTitles.forEach(title -> softAssertions.assertThat(title.toLowerCase())
                .as("Wrong product title")
                .contains(expectedResult.toLowerCase()));

        softAssertions.assertAll();
    }

    @And("I validate response product structure contains:")
    public void iValidateResponseProductStructure(DataTable table) {
        final SoftAssertions softAssertions = new SoftAssertions();
        final Response response = SerenityRest.then().extract().response();

        response.jsonPath().getList("", Map.class)
                .forEach(product ->
                        table.rows(0).asList().forEach(field ->
                                softAssertions.assertThat(product.containsKey(field))
                                        .as("Missing product filed").isTrue()));

        softAssertions.assertAll();
    }

    @And("I verify error message is correct")
    public void iVerifyErrorMessageIsCorrect() {
        Response response = SerenityRest.then().extract().response();
        Map<String, Object> errorResponse = response.getBody().jsonPath().get("detail");

        Assertions.assertThat(errorResponse.get("error"))
                .as("Missing error statement")
                .isEqualTo(true);
        Assertions.assertThat(errorResponse.get("message"))
                .as("Wrong error text")
                .isEqualTo("Not found");
    }
}

