@search @all
Feature: Search for the product
### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  @all
  Scenario Outline: search for a product and check title contains search param
    When I call search endpoint "<product>"
    Then I verify that response status is: 200
    And I verify that response contains products with title "<product>"

    Examples:
     | product |
     | apple   |

## these tests are ignored on bug: https://gitlab.com/dmitriy_kovalenko/leaseplantestgroup/-/issues/1
#    Examples:
#     | mango   |
#     | tofu    |
#     | water   |

  Scenario: search for a not existing product
    When I call search endpoint "car"
    Then I verify that response status is: 404
    And I verify error message is correct

  Scenario Outline: search for a product and check response structure is valid
    When I call search endpoint "<product>"
    Then I verify that response status is: 200
    And I validate response product structure contains:
      | provider     |
      | title        |
      | url          |
      | brand        |
      | price        |
      | unit         |
      | isPromo      |
      | promoDetails |
      | image        |

    Examples:
      | product      |
      | apple        |
      | mango        |
      | tofu         |
      | water        |